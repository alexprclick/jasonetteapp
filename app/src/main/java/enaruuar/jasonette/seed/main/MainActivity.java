package enaruuar.jasonette.seed.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.onesignal.OneSignal;

import enaruuar.jasonette.seed.Core.JasonViewActivity;
import enaruuar.jasonette.seed.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button buttonhome;
    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("enaruuar.jasonette.seed", MODE_PRIVATE);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        buttonhome = (Button) findViewById(R.id.buttonhome);
        buttonhome.setOnClickListener((View.OnClickListener) this);


        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/14451.ttf");
        buttonhome.setTypeface(face);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonhome:
                Intent intenthome = new Intent(this, JasonViewActivity.class);
                startActivity(intenthome);
                break;
            default:
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstrun", true)) {
            // При первом запуске (или если юзер удалял все данные приложения)
            // мы попадаем сюда. Делаем что-то
//и после действия записывам false в переменную firstrun
//Итого при следующих запусках этот код не вызывается.


            prefs.edit().putBoolean("firstrun", false).commit();
        }
        else {
            Intent intenthome = new Intent(this, JasonViewActivity.class);
            startActivity(intenthome);
        }
    }

}




